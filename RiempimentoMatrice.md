# Riempimento matrice #

Pur usando un [servizio web che fornisce una matrice di distanze](http://project-osrm.org/docs/v5.5.2/api/#table-service) tra punti individuati sulla mappa, è possibile che il numero massimo di punti gestiti dal servizio superi il numero che si ha necessità di utilizzare. Nel seguito viene descritto qualche metodo per ovviare il problema utilizzando richieste multiple.

## Suddivisione in blocchi con un numero primo di punti ##

Sia *n* il numero di punti totale per il quale sia necessario calcolare l'intera matrice delle distanze reciproche.

Si trovi *p* il minor numero primo tale che *p&middot;p >= n*.
Ognuno dei numeri da *0* a *n* potrà essere scritto nella forma:

(1) *x=q&middot;p+r* con *0 <= q < p* e *0 <= r < p*.

a) Per ogni possibile valore di *q* (al più *p*) si effettua una chiamata al servizio, per ottenere la matrice delle distanze dei *p* punti (per il valore maggiore possono essere meno) che rappresentati come nell'equazione (1) condividono il medesimo valore *q*.

b) Quindi si effettuano altre *p&middot;p* chiamate al servizio selezionando al variare dei due indici *i* e *j* tra *0* e *p-1* tutti i punti *x=q&middot;p+r* per i quali vale *r=q&middot;i+j (mod p)*.

In questo modo con al più *p&middot;(p+1)* chiamate, ognuna con al più *p* punti, si ottiene dal servizio la distanza tra ognuna delle coppie degli *n* punti iniziali. Inoltre per ognuna di tali coppie l'informazione verrà richiesta al servizio esattamente una sola volta, evitando di sovraccaricare il servizio esterno con richieste superflue.

Di quest'ultima proprietà è possibile dare la dimostrazione rigorosa che segue.

Siano *0 <= x < y < n* due indici distinti che individuano due degli *n* punti iniziali. Sarà possibile scriverle entrambe seguendo l'equazione (1) come *x = q&middot;p + r, y = Q&middot;p + R*.

Se *q = Q*, la distanza tra i due punti è stata richiesta al servizio esterno in una delle chiamate effettuate al punto a).

Altrimenti, è possibile, poiché *p* è primo, trovare l'unico *i < p* tale che *i&middot;(Q-q) = R-r (mod p)*, quindi *j < p* tale che *j=r - q&middot;i (mod p)*. È facile verificare che valgono sia *r=q&middot;i+j (mod p)*, sia *R=Q&middot;i+j= (Q-q)&middot;i + q&middot;i+j = R-r + r (mod p)*, pertanto la distanza tra i due punti è stata richiesta nella sola chiamata con i relativi valori *i* e *j* al punto b).
