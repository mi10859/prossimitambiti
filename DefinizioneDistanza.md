# Definizione di una distanza #

## Una possibile definizione di distanza tra due ambiti ##

Ogni ambito rappresenta un'area, un insieme di scuole.

Gli ambiti non sono quindi puntiformi e la loro distanza non è definita in modo ovvio.

Si potrebbe essere tentati dalla possibilità di selezionare un singolo punto come rappresentativo di un ambito per calcolare le distanze a partire da quello. L'impressione è che questa idea porti ad una eccessiva semplificazione, non permettendo di tenere in considerazione l'effettiva estensione degli ambiti.

Altra tentazione potrebbe essere quella di considerare la distanza minima: se dovessimo dire quanto è distante Torino dalla Francia, misureremmo la strada più breve... Ne deriverebbe che la distanza tra aree confinanti è **zero**; questo da un lato non ci piace perché non ci aiuta a distinguere, tra tante aree confinanti, quale sia la più vicina. Dall'altro non è affatto convincente quando il confine tra due aree è posto su una barriera naturale, come il crinale di una montagna.

Una classica estensione del concetto di distanza a insiemi o aree è la [distanza di Hausdorff](https://it.wikipedia.org/wiki/Distanza_di_Hausdorff); questa sicuramente permetterebbe di definire una metrica nel senso matematico, ma potrebbe non fornire una corretta stima di quella che è percepibile come distanza tra due ambiti.

Abbiamo quindi pensato di definire la distanza tra due ambiti *A* e *B* come il massimo, sulle coppie di scuole, *a* in *A* e *b* in *B*, della distanza tra la scuola *a* e la scuola *b*.

Questa definizione, mettendo anche a punto i dettagli, permette di
calcolare operativamente una distanza tra due ambiti, che gode della
proprietà triangolare, è simmetrica e sempre positiva, ma non gode dell'altra
proprietà che ci si aspetterebbe da una distanza:
prendendo un singolo ambito *A*, la distanza dell'ambito con se stesso
non risulta zero, ma il diametro (la distanza tra i due punti più
lontani) dell'ambito stesso, tanto che paradossalmente può capitare
che un ambito non risulti essere il più vicino a sé stesso tra gli
ambiti, come vedremo in seguito.

### Differenza tra Hausdorff e altra definizione ###

Proviamo a vedere il diverso comportamento su alcuni casi ipotetici o concreti.
Si prenda per esempio il primo schema in figura, costituito dal semicerchio rosso, il semicerchio blu
e il quadrato verde che condividono la misura rispettivamente del raggio, del diametro e del lato.

![Primo schema esemplificativo](./EsempioAree.svg)

La domanda è: quale delle due aree è più vicina al semicerchio rosso?
Per la distanza di Hausdorff sono equidistanti, prendendo invece in considerazione la distanza massima
tra coppie di punti, il quadrato sarà più distante.

Come già accennato, mentre la distanza di Hausdorff di un'area da se stessa è
(coerentemente) nulla, per la distanza massima questo non è vero. Si prenda per
esempio il secondo schema in figura, costituito dal semicerchio rosso e il
semicerchio blu, con lo stesso centro, il primo con raggio doppio rispetto
all'altro.

![Secondo schema esemplificativo](./EsempioAreeDue.svg)

Con la distanza massima, il semicerchio rosso, che ha il diametro maggiore,
risulta più distante da sé che dall'altro semicerchio.

Come esempio concreto si può invece esaminare quel che risulta calcolando
le distanze relative ad uno degli ambiti territoriali del Piemonte:
l'ambito Vercelli2. Questo ambito, per la peculiare forma della provincia di
Vercelli, si trova ad essere un sottile corridoio che quasi *circonda* gli
ambiti di Biella con molte scuole vicinissime alla provincia di Novara.

Nelle immagini che seguono, le istituzioni scolastiche sono segnate con un
cerchietto di colore che dipende dall'ambito di appartenenza.

Nella prima sono disegnati anche i segmenti che uniscono la coppia di scuole
che determina la distanza di Hausdorff tra l'ambito Vercelli2 e gli ambiti
*viciniori*, nell'ordine: Biella2, Novara2, Novara1, Biella1 e Vercelli1. 

![Mappa ambito VC2 con distanze di Hausdorff](./Hf16.png)

Si noti che la lunghezza del segmento, disegnato *in linea d'aria* non è
proporzionale alla distanza considerata: infatti il segmento disegnato per
Biella2, pur corto, rappresenta un tragitto con tempi di percorrenza così
elevati da risultare la quarta distanza.

Nella seconda immagine sono disegnati i segmenti che uniscono la coppia
di scuole che determina la distanza massima tra l'ambito Vercelli2 e gli
ambiti *viciniori*, nell'ordine:
Biella2, Novara2, Biella1, Novara1, *Vercelli2* e Vercelli1.

![Mappa ambito VC2 con distanze MaxMax](./MM16.png)

Si noti che in questo caso la distanza dell'ambito Vercelli2 da sé stesso
(il suo diametro) è tale da superare quella da quattro dei cinque
altri ambiti considerati.

Pur misurando distanze diverse, escludendo l'ambito stesso, solo le
posizioni di Biella1 e Novara1 si scambiano.

## Il *diametro* del sistema scuole in Piemonte ##

Per gli ambiti piemontesi, la massima distanza sarà limitata dalla massima
distanza tra due istituzioni scolastiche nella regione.

Questa è realizzata da due scuole nei comuni di Demonte e Baceno. La loro distanza (misurata come strada da percorrere per andare dall'una all'altra) è di oltre 300 Km per 4 ore di viaggio.

[![Diametro del Piemonte](diametroPiemonte.jpeg)](http://www.openstreetmap.org/directions?engine=osrm_car&route=46.2582%2C8.3213%3B44.3155%2C7.293)

Considerando la distanza massima, questa sarà l'effettiva distanza presa in considerazione tra i due ambiti che rispettivamente includono le due scuole.

## La distanza tra scuole ##

La distanza *in linea d'aria* non sembra significativa, con questa distanza si rischierebbe di sottostimare l'effettivo peso di uno spostamento tra scuole separate da barriere naturali. Serve quindi misurare la distanza su percorsi che collegano le scuole.

Anche questa distanza potrebbe non essere una metrica nel senso matematico, se misuriamo la distanza tra la scuola *a* e la scuola *b* con la lunghezza del percorso che collega la prima alla seconda, tale distanza potrebbe non essere simmetrica (la distanza da *a* a *b* potrebbe essere diversa dalla distanza da *b* ad *a*) a causa di porzioni del tragitto per i quali solo un senso di percorrenza è possibile.

Se invece della lunghezza si stima il tempo necessario per spostarsi da una scuola all'altra, alle possibili ragioni di asimmetria si aggiungono le differenze rilevanti di quota.

Pertanto, decidendo di utilizzare una distanza basata sulla percorrenza lungo strade, sarà opportuno calcolare entrambe le percorrenze, da *a* a *b* e viceversa, per poi considerare come distanza la somma (o la media) tra questi due valori. Oltre al vantaggio di ottenere un valore per definizione simmetrico, avremo anche dal punto di vista intuitivo un valore più rappresentativo, che terrà conto della peso sia dell'andare che del tornare.

## Le catene non sono simmetriche ##

Da sfatare l'idea che le catene di prossimità debbano essere simmetriche.

Si dice: se abbiamo in ordine *A*, *B*, *C* e *D*, allora dall'altra parte avremo *D*, *C*, *B* ed *A*. Ciò non è vero in generale, ma solo in casi molto particolari.

Supponiamo ad esempio di avere su una linea *B:A::C::D*. Partendo da *A*, potremmo dire che le distanze determinano proprio l'ordine descritto prima, quello alfabetico, ma partendo da *D* l'ordine non sarebbe quello inverso, *A* è posizionato tra *C* e *B*.

Se gli esempi si cercano in due dimensioni, la situazione diventa ancora più complicata e nemmeno scegliendo due "estremi" in insiemi di almeno quattro punti si avrà la garanzia che le catene siano simmetriche.