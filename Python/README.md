# Codice e dati per il calcolo delle distanze tra ambiti #

## Contenuto di questa cartella ##

* Codice, lo script Python per il calcolo delle distanze: [computoDistanzeAree.py](computoDistanzeAree.py).
* Dati, sulle istituzioni scolastiche in Piemonte : [DatiScuole2016-17.csv](DatiScuole2016-17.csv).

## Codice ##

Lo script [computoDistanzeAree.py](computoDistanzeAree.py) è stato scritto per calcolare
la distanza tra ambiti costituiti da un insieme di istituzioni scolastiche.

È stato scritto per il linguaggio Python versioni 3.*x* e per funzionare richiede alcuni
dati relativi ad ambiti e scuole nel file 'DatiScuole2016-17.csv', nel formato in seguito
specificato, e la possibilità di accedere tramite rete ('http su porta 80') alle
[API di OSRM](http://project-osrm.org/docs/v5.5.2/api/#table-service).

Lo script legge i dati dal file CSV, interroga OSRM per costruire una matrice di distanze
e salva tre file CSV:

* la matrice delle distanze fornite da OSRM tra le istituzioni scolastiche;
* la lista delle istituzioni con l'indicazione delle coordinate e del nome segnalato da OSRM;
* le catene di *viciniorietà* per tutti gli ambiti, con due diverse definizioni di distanza e la corrispondente coppia di scuole che la determina.

Lo script ha attualmente una gestione degli errori molto rudimentale, non tutti gli
eventi che possono portare ad un risultato non attendibile vengono adeguatamente segnalati.

### Licenza d'uso ###

Lo script è rilasciato con la licenza [AGPL-v3+](../LICENCE.md), ad una prima superficiale
analisi tale licenza è risultata compatibile con quelle delle librerie utilizzate, ma
una analisi approfondita in tal senso deve ancora essere effettuata.

## Dati ##

Il file [DatiScuole2016-17.csv](DatiScuole2016-17.csv) contiene una anagrafica delle
istituzioni scolastiche statali del Piemonte. Le informazioni contenute in questo file
sono molte di più di quelle strettamente necessarie per il funzionamento del codice
per il calcolo delle distanze.

Per il funzionamento del codice, è necessario che il file csv contenga nella prima
riga le intestazioni delle colonne e per ogni riga le informazioni su una istituzione
scolastica. Le colonne sono separate dal simbolo *punto e virgola* (;). Le colonne
utilizzate dal programma, senza le quali lo script si fermerebbe, sono:

* **Scuola**, un identificatore unico per ogni istituzione (es. il codice meccanografico);
* **Ambito**, un identificatore dell'ambito del quale l'istituzione fa parte;
* **Lat**, la localizzazione geografica della scuola: latitudine;
* **Long**, longitudine;

La geo-referenziazione (latitudine e longitudine) è fornita secondo il sistema WGS84
(ESPG:4326) che è il sistema utilizzato dal servizio web (OSRM) utilizzato dal codice.
Per il funzionamento dello script, è essenziale l'uso dello stesso sistema di
coordinate geografiche

Tutte le altre colonne, sono fornite per completezza di informazione e per facilitare
le verifiche della corretta geo-referenziazione.

### Licenza d'uso ###

I dati provengono in parte dai dati 2015/16 pubblicati sul portale
[Scuola in Chiaro](http://cercalatuascuola.istruzione.it/cercalatuascuola/opendata/),
che sono liberamente utilizzabili.

Su questi dati di partenza sono state effettuate verifiche, correzioni, elaborazioni
e aggiornamenti, usando tra l'altro i dati della [Regione
Piemonte](http://www.geoportale.piemonte.it/geonetworkrp/srv/ita/metadata.show?id=2751&currTab=rndt).
Il complesso di queste operazioni rende la nuova base di dati
tutelabile.

La base di dati sugli istituti scolastici del Piemonte è quindi rilasciata con la
doppia licenza [ODbL](http://opendatacommons.org/licenses/odbl/) oppure
[CC-BY-SA-v4](https://creativecommons.org/licenses/by-sa/4.0/deed.it). Ciò significa
che siete autorizzati ad utilizzarli se rispettate i termini di almeno una delle
due licenze, oppure entrambe.

### Esempio d'uso ###

[Umap](http://u.osmfr.org/m/123303/)