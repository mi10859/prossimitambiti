#!/usr/bin/env python
# -*- coding: utf-8 -*-

##en
#    computoDistanzeAree, computes distances of sets of points
#    Copyright (C) 2016, 2017  Marco BODRATO
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##it
#    computoDistanzeAree, analizza le distanze tra insiemi di punti
#    Scritto nel 2016-2017 da Marco BODRATO, che ne detiene i diritti d'autore
#
#    Questo programma è software libero: può essere ridistribuito o modificato
#    nei termini della licenza "GNU Affero General Public License" come
#    pubblicata dalla Free Software Foundation; o la versione 3 della
#    licenza, o (a vostra scelta) qualunque versione successiva.
#
#    Questo programma è distribuito con l'auspicio che sarà utile,
#    ma SENZA ALCUNA GARANZIA; senza neppure la garanzia implicita di
#    VENDIBILITÀ o ADEGUATEEZZA AD UN PARTICOLARE SCOPO. Si veda la
#    GNU Affero General Public License per ulteriori dettaglio.
#
#    Dovreste aver ricevuto una copia della GNU Affero General Public License
#    con a questo programma. Vedere altrimenti <http://www.gnu.org/licenses/>.
#

import csv
import requests
from math import sqrt, ceil
from operator import itemgetter

class Errore(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def nextprime (n):
    """Naively search for the smallest prime natural number >= n > 0"""
    if n < 3:
        return 2
    n |= 1
    d = 3
    while d*d <= n:
        if n%d == 0:
            d = 1
            n += 2
        d += 2
    return n

def checkdim (dim):
    """Verifica metodo di riempimento matrice con suddivisione in blocchi
    con un numero primo di punti."""
    matriceDistanze = bytearray(dim*dim);
    dimBlocco = nextprime (int(ceil(sqrt(dim))))
    chiamate = 0
    for i in range (0, dim, dimBlocco):
        v = range (i, min (dim, i+dimBlocco))
        if len (v) == 1:
            matriceDistanze [v[0]*(dim+1)] += 1
        else:
            chiamate += 1
            for j in v:
                for k in v:
                    if j != k:
                        matriceDistanze [j*dim+k] += 1
                    else:
                        matriceDistanze [j*dim+k] += 1
    for d in range (0, dimBlocco):
        for s in range (0, dimBlocco):
            v = [s]
            for i in range (1, dimBlocco):
                j = i * dimBlocco + (s + d * i) % dimBlocco
                if j >= dim:
                    break
                v.append (j)
            chiamate += 1
            for j in v:
                for k in v:
                    if j != k:
                        matriceDistanze [j*dim+k] += 1
    for i in matriceDistanze:
        if i != 1:
            return -1
    return chiamate

def distanza (p1, p2):
    """Distanza grossolana in chilometri tra i due punti p1 e p2, decente
    solo per distanze inferiori alla decina di chilometri e solo
    attorno al 45° parallelo. La funzione calcola una "distanza" in
    gradi poi rapportata ai 40000 km di circonferenza, sfruttando il
    fatto che sin(45°)^2 = cos(45°)^2 = 1/2...
    Per i due punti si ha p[0]=lon, p[1]=lat.
    """
    return sqrt((p1[0]-p2[0])**2/2+(p1[1]-p2[1])**2)*40000/360

def generaURL (punti, indici):
    """Genera URL per richiesta a servizio esterno selezionando nella
    lista dei punti, quelli specificati nella lista degli indici.
    """
    url = 'http://router.project-osrm.org/table/v1/driving/'
    for punto in indici:
        url += repr(punti[punto][0][0])
        url += ','
        url += repr(punti[punto][0][1])
        url += ';'
    return url[:-1]

def leggiDatiIniziali (nomeFileDati):
    separatoreDati = ';'
    try:
        csvDatiIniziali = csv.DictReader (open (nomeFileDati), delimiter = separatoreDati)
        print ('Leggo i dati iniziali dal file', nomeFileDati)
    except:
        print ('Errore nella lettura del file', nomeFileDati, '.')
        raise Errore ('Errore nella lettura del file.')
    #
    if not (('Ambito' in csvDatiIniziali.fieldnames) and ('Scuola' in csvDatiIniziali.fieldnames) and ('Lat' in csvDatiIniziali.fieldnames) and ('Long' in csvDatiIniziali.fieldnames)):
        print ('I nomi attesi delle colonne (Ambito,Scuola,Lat,Long) non sono stati trovati tra i nomi presenti nel file', nomeFileDati, 'che sono:', csvDatiIniziali.fieldnames)
        raise Errore('I nomi attesi delle colonne (Ambito,Scuola,Lat,Long) non sono stati trovati')

    zone = {}
    punti = {}

    for rigaPunto in csvDatiIniziali:
        area = rigaPunto['Ambito']
        punto = rigaPunto['Scuola']
        longitudine = float (rigaPunto['Long'].replace(',','.'))
        latitudine = float (rigaPunto['Lat'].replace(',','.'))
        if punto in punti:
            print ('Punto', punto, 'duplicato.')
        else:
            punti[punto] = [(longitudine, latitudine)]
        if not (area in zone):
            zone[area] = set ();
        zone[area] |= {punto};
    return zone, punti

def ordinaPunti (zone, punti):
    risultato = []
    for area in sorted (zone):
        print (area, ':', len (zone[area]))
        for punto in sorted (zone[area]):
            print (punto, ':', punti[punto])
            risultato.append (punto)
    return risultato

def ottieniMatriceDaServizio (punti, etichettePunti):
    url = generaURL (punti, etichettePunti)
    risposta = requests.get(url)
    if risposta.status_code != 200:
        print ('Sito non disponibile o URL errato.')
        raise Errore ('Sito non disponibile o URL errato.')

    try:
        risposta = risposta.json()
    except:
        risposta = {}

    if risposta.pop('code') != 'Ok':
        print ('Il sito non ha potuto fornire la risposta')
        raise Errore  ('Il sito non ha potuto fornire la risposta')

    if risposta.pop('destinations') != risposta['sources']:
        print ('Destinazioni e partenze sono diverse')
        raise Errore ('Destinazioni e partenze sono diverse')

    for p, c in zip (risposta['sources'], etichettePunti):
        puntoRivisto = (p['location'][0], p['location'][1], p['name'])
        if not puntoRivisto in punti[c][1:]:
            punti[c].append(puntoRivisto)
    return risposta['durations']

def ottieniMatrice (punti, etichettePunti):
    def _richiedi_sotto_matrice (punti, etichettePunti, indici):
        if len (indici) != 1:
            sottoMatrice = ottieniMatriceDaServizio (punti, [etichettePunti[i] for i in indici])
            for j in range (len (v)):
                for k in range (len (v)):
                    matrice [v[j]][v[k]] = sottoMatrice [j][k]        
    dim = len (etichettePunti)
    if dim < 80:
        return  ottieniMatriceDaServizio (punti, etichettePunti)
    matrice = [[0 for x in etichettePunti] for x in etichettePunti]
    dimBlocco = nextprime (int(ceil(sqrt(dim))))
    for i in range (0, dim, dimBlocco):
        v = range (i, min (dim, i+dimBlocco))
        try:
            _richiedi_sotto_matrice (punti, etichettePunti, v)
        except:
            print ('Errore fase a), con i =', i)
            return matrice
    for d in range (0, dimBlocco):
        for s in range (0, dimBlocco):
            v = [s]
            for i in range (1, dimBlocco):
                j = i * dimBlocco + (s + d * i) % dimBlocco
                if j >= dim:
                    break
                v.append (j)
            try:
                _richiedi_sotto_matrice (punti, etichettePunti, v)
            except:
                print ('Errore fase b), con d =', d, ' s =', s)
                return matrice
    return matrice

def distanzaPuntiIndici (matriceDistanze, i, j):
    return (matriceDistanze [j][i] + matriceDistanze [i][j])/2

def distanzaPunti (matriceDistanze, puntiOrdinati, primo, secondo):
    if not (primo in puntiOrdinati) and (secondo in puntiOrdinati):
        return -1
    return distanzaPuntiIndici (matriceDistanze, puntiOrdinati.index(primo), puntiOrdinati.index(secondo))

def distanzaHausdorff (matriceDistanze, puntiOrdinati, prima, seconda):
    def _distanza_Hausdorff_parziale (matriceDistanze, prima, seconda):
        return max([min([(distanzaPuntiIndici (matriceDistanze, x, y),x,y) for x in seconda]) for y in prima])
    v1 = [puntiOrdinati.index(x) for x in prima]
    v2 = [puntiOrdinati.index(x) for x in seconda]
    return max(_distanza_Hausdorff_parziale (matriceDistanze, v1, v2),_distanza_Hausdorff_parziale (matriceDistanze, v2, v1))

def distanzaMaxMax (matriceDistanze, puntiOrdinati, prima, seconda):
    v1 = [puntiOrdinati.index(x) for x in prima]
    v2 = [puntiOrdinati.index(x) for x in seconda]
    return max([max([(distanzaPuntiIndici (matriceDistanze, x, y),x,y) for x in v1]) for y in v2])

def distanzaMedia (matriceDistanze, puntiOrdinati, prima, seconda):
    v1 = [puntiOrdinati.index(x) for x in prima]
    v2 = [puntiOrdinati.index(x) for x in seconda]
    return sum([sum([distanzaPuntiIndici (matriceDistanze, x, y) for x in v1]) for y in v2]) / (len(v1)*len(v2))

ambiti, scuole = leggiDatiIniziali ('DatiScuole2016-17.csv')
scuoleOrdinate = ordinaPunti (ambiti, scuole)
matriceDistanze = ottieniMatrice (scuole, scuoleOrdinate)

distanzeAmbiti={}
distanzeHausdorff={}
distanzeMedia={}
for a in ambiti:
    distanzeAmbiti[a]= sorted ([(b,distanzaHausdorff(matriceDistanze, scuoleOrdinate, ambiti[a],ambiti[b]), distanzaMaxMax(matriceDistanze, scuoleOrdinate, ambiti[a],ambiti[b])) for b in ambiti], key=itemgetter(2))
    distanzeHausdorff[a]=sorted(distanzeAmbiti[a], key=itemgetter(1))
    distanzeMedia[a]= sorted ([(b,distanzaMedia(matriceDistanze, scuoleOrdinate, ambiti[a],ambiti[b])) for b in ambiti], key=itemgetter(1))

separatoreDati = ';'
with open('distanzeAmbiti.csv', 'w', newline='') as csvRisultati:
    risultati = csv.writer (csvRisultati, delimiter = separatoreDati, quoting = csv.QUOTE_MINIMAL)
    risultati.writerow (['Ambito','StileDistanza','Catena','Ordine','Misura','Scuola1','Scuola2','WKT'])
    for a in sorted(ambiti):
        print('Catena per ambito', a, 'con distanza max:max :')
        i = 0
        for x in distanzeAmbiti[a]:
            am = x[0]
            d = x[2][0]
            s1 = scuoleOrdinate[x[2][1]]
            s2 = scuoleOrdinate[x[2][2]]
            print (am, '(d=', ceil(d), ',', s1, scuole[s1][0], '<->', s2, scuole[s2][0], ')')
            WKT = 'LINESTRING (' + repr(scuole[s1][0][0]) + ' ' + repr(scuole[s1][0][1]) + ','
            WKT += repr(scuole[s2][0][0]) + ' ' + repr(scuole[s2][0][1]) + ')'
            risultati.writerow ([a,'MaxMax',am,i,d,s1,s2,WKT])
            i = i + 1
        print('Catena per ambito', a, 'con distanza di Hausdorff :')
        i = 1
        for x in distanzeHausdorff[a][1:]:
            am = x[0]
            d = x[1][0]
            s1 = scuoleOrdinate[x[1][1]]
            s2 = scuoleOrdinate[x[1][2]]
            print (am, '(d=', ceil(d), ',', s1, scuole[s1][0], '<->', s2, scuole[s2][0], ')')
            WKT = 'LINESTRING (' + repr(scuole[s1][0][0]) + ' ' + repr(scuole[s1][0][1]) + ','
            WKT += repr(scuole[s2][0][0]) + ' ' + repr(scuole[s2][0][1]) + ')'
            risultati.writerow ([a,'Hausdorff',am,i,d,s1,s2,WKT])
            i = i + 1
        print('Catena per ambito', a, 'con distanza media :')
        i = 0
        for x in distanzeMedia[a]:
            am = x[0]
            d = x[1]
            print (am, '(d=', ceil(d), ')')
            risultati.writerow ([a,'Media',am,i,d])
            i = i + 1

with open('distanzeScuole.csv', 'w', newline='') as csvDistanze:
    distanzeScuole = csv.writer (csvDistanze, delimiter = separatoreDati, quoting = csv.QUOTE_MINIMAL)
    distanzeScuole.writerow (['Scuole'] + scuoleOrdinate)
    for s, r in zip (scuoleOrdinate, matriceDistanze):
        distanzeScuole.writerow ([s] + r)

with open('coordinateScuole.csv', 'w', newline='') as csvScuole:
    coordinateScuole = csv.writer (csvScuole, delimiter = separatoreDati, quoting = csv.QUOTE_MINIMAL)
    coordinateScuole.writerow (['Ambito','Scuola','Long','Lat','Long_sito','Lat_sito','nome_sito'])
    for s in scuole:
        ambito = [a for a in ambiti if s in ambiti[a]] [0]
        coordinateScuole.writerow ([ambito,s,scuole[s][0][0],scuole[s][0][1],scuole[s][1][0],scuole[s][1][1],scuole[s][1][2]])
